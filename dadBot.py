#!/usr/bin/env python3
import sys
from os import environ, path

from pocketsphinx import *
from sphinxbase.sphinxbase import *
import tts
from jokeLib import dadJokes
from random import randint
#speech = LiveSpeech( lm = False, keyphrase = 'im tired', kws_threshold = 1e-20)
#for phrase in speech:
#    print(phrase.segments(detailed = True))


dadJoke = dadJokes()
dadJoke = dadJoke.dadJoke


def getDadJoke():
    min = 0
    max = len(dadJoke)-1
    randI = randint(min, max)
    return dadJoke[randI]

model_path = get_model_path()
data_path = get_data_path()
config = {
        'verbose'  : True,
        'buffer_size' : 2048,
        'no_search' : False,
        'hmm' : os.path.join(model_path, 'en-us'),
        'lm' : os.path.join(model_path, 'en-us.lm.bin'),
        'dict' : os.path.join(model_path, 'cmudict-en-us.dict')
        }

speech = LiveSpeech(**config)

for phrase in speech:
    arr = phrase.segments()
    if "i'm" in arr:
        index = arr.index("i'm")
        next_val = arr[index + 1]
        while (next_val == "a" or next_val == "the" or next_val == "an"):
            index += 1
            next_val = arr[index + 1]
        tts._main("hi  " + arr[index + 1] + " i'm dad bot!")
        print(arr[index], arr[index + 1])
    elif "joke" in arr:
        tts._main(getDadJoke())

















